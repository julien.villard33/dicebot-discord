# Discord DiceBot

This is a simple bot to roll dice on your Discord server !

Feel free to use the it, share it and modify it !

## Usage

You can click on this link to add the bot on your server :
[Add the bot to your server](https://discordapp.com/api/oauth2/authorize?client_id=702558116654350467&scope=bot&permissions=3072)

## Permissions

The bot will only ask for 2 permissions :
* Reading channels
* Writing on channels

## Commands

You can use the command **!dice** to see the different dice commands available.

*Replace the stars with any numbers between 1 and 99.*

You can roll dice by writing in the discord chat : `!*d*`

You can add a **+** modifier : `!*d*+*`

You can add a **-** modifier : `!*d*-*`

Here are a few examples :
> * !d8
> * !2d8
> * !1d20+4
> * !1d20-4


For a d20 roll with a +4 modifier, ***!1d20+4***, the expected output is the following :

![Screenshot](img/dice_example.png)

## Bot creation

This bot was created using the Node.js module [DiscordJS](https://discord.js.org/#/) to interact easily with the Discord API.

## Issues

If you encounter any issue please send me a [mail](mailto:contact@julien-villard.tech) describing the problem !

## Licence

MIT

***Copyright (c) 2020 Julien Villard***