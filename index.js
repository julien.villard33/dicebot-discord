/*
    Bot created by Julien 'Keup' Villard
*/


// init dependencies
const Discord = require('discord.js');

// config (not useful on heroku)
//const config = require("./config.json");

// create client
const Dicebot = new Discord.Client();

// wait for bot to be ready
Dicebot.on('ready', () => {
    console.log('Bot ready!');
});

// prefix definition
const prefix = "!";

// global var init
let diceN = [];

// command help answer
let diceR =
`Here are the dice **commands** you can use :
\`\`\`Classic roll : ${prefix}*d*\`\`\`
\`\`\`With + modifier : ${prefix}*d*+*\`\`\`
\`\`\`With - modifier : ${prefix}*d*-*\`\`\`
Replace the **stars** by numbers (by default if nothing is written **before** the *d* it will be 1)`;

// math function to rand dice and add them
function sum(array) {
    let sum = 0;
    for (let i = 0; i < array[0]; i++) {
        let rand = getRandomInt(array[1])
        diceN.push(rand);
        sum = sum + rand;
    }
    return sum;
}

// math random function
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max)) + 1;
}

// regex dice detector
// dice format : *d*
const regexDice_NdN = /^!\d{0,2}d\d{1,2}/;
// dice format + modifier : *d6+
const regexDice_NdNp = /^!\d{0,2}d\d{1,2}[+-]/;
// modifier
const regexP = /[+]/;

// embed answer help
const embedHelp = new Discord.MessageEmbed()
    .setTitle("Dice helper on the way !")
    .setAuthor("DiceBot")
    .setColor("#00AE86")
    .setDescription(diceR)
    .setTimestamp()
    .setFooter("It was my pleasure to help you !");

// event listener for messages
Dicebot.on('message', message => {
    // prevent listening on all messages or other bot messages
    if (!message.content.startsWith(prefix) || message.author.bot) return;
    // help message
    if (message.content.startsWith(prefix + 'dice')) {
        // send answer
        message.channel.send(embedHelp);
        // detect regex for dice *d*
    } else if (regexDice_NdN.exec(message.content)) {
        // strip the !
        let dice = message.content.substring(1);
        // init var
        let modifier = 0;
        let modifierSplit = [];
        let modifierP = true
        let diceResult;
        // reset global var value
        diceN = [];
        // verify if there is modifiers
        if (regexDice_NdNp.exec(message.content)) {
            if(regexP.exec(message.content)){
                // strip the + modifier
                modifierSplit = dice.split('+');
            }else{
                // strip the - modifier
                modifierSplit = dice.split('-');
                modifierP = false;
            }
            // in case modifier is not set after the +
            if(modifierSplit[1] != ''){
                modifier = modifierSplit[1];
            }
            dice = modifierSplit[0];
        }
        // split the dice
        let diceSplit = dice.split('d');
        // init var
        let diceRand = '';
        let diceTotal = '';
        // case *d*
        if (diceSplit[0] != '') {
            // get total value
            diceRand = sum(diceSplit);
        // case d*
        }else{
            diceRand = getRandomInt(diceSplit[1]);
        }
        // add dice value + modifier
        if(modifierP){
            diceTotal = parseInt(diceRand)+parseInt(modifier);
            modifier = `+${modifier}`;
        }else{
            diceTotal = parseInt(diceRand)-parseInt(modifier);
            modifier = `-${modifier}`;
        }
        // if total is less than one modify it to be at 1
        if(diceTotal<1){
            diceTotal =1;
        }
        // result formating for embed message
        if(diceSplit[0] == ''){
            diceSplit[0] = 'a';
            diceResult = diceRand;
        }else{
            diceResult = diceN.join(' / ');
        }
        // dice embed message
        const embedDice = new Discord.MessageEmbed()
            .setTitle(`${message.author.username} rolled ${diceSplit[0]} dice ${diceSplit[1]}`)
            .setColor("#00AE86")
            .addField(`Dice result :`,`${diceResult}`)
            .addField(`Modifier : ${modifier}`,`__Total__ : **${diceTotal}**`)
        message.channel.send(embedDice);
    }
});
// bot connection (token must be hidden in config file)
Dicebot.login(process.env.BOT_TOKEN);